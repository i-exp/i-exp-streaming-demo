package ru.iexp.arsdkdemo;


import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.SwitchPreference;

import java.util.Arrays;

public class PrefsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.prefs, rootKey);
        setPreferenceChangeListener(findPreference(getString(R.string.pref_signaling_socket)));
        setPreferenceChangeListener(findPreference(getString(R.string.pref_manager_socket)));
        setPreferenceChangeListener(findPreference(getString(R.string.pref_use_qr_code)));
        setPreferenceChangeListener(findPreference(getString(R.string.pref_use_manager)));
        setPreferenceChangeListener(findPreference(getString(R.string.pref_use_lighting)));
        setPreferenceChangeListener(findPreference(getString(R.string.pref_use_autofocus)));
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        // now supporting only string prefs
        if (preference instanceof EditTextPreference) {
            // todo subclass (SocketPref extends EditTextPreference or smth)
            if (preference.getKey().equals(getString(R.string.pref_manager_socket))) {
                if (!validateSocketString(newValue.toString())) {
                    Toast.makeText(this.getContext(),
                            R.string.err_wrong_socket_format, Toast.LENGTH_LONG).show();
                    return false;
                }
            }
            preference.setSummary(newValue.toString());
        } else if (preference instanceof SwitchPreference) {
	        if (preference.getKey().equals(getString(R.string.pref_use_qr_code))){
	        	    findPreference(getString(R.string.pref_dynamic_tracking)).setEnabled(Boolean.valueOf(newValue.toString()));
	        }
        }
        return true;
    }

    private boolean validateSocketString(String socketString) {
        try {
            // todo use apache utils or guava to validate if it will be needed more than once
            String[] addr = socketString.split(":");
            int port = Integer.parseInt(addr[1]);
            if (port <= 0 || port > 65535) {
                return false;
            }
            String[] groups = addr[0].split("\\.");
            if (groups.length != 4) {
                return false;
            }
            return Arrays.stream(groups)
                    .filter(s -> s.length() >= 1)
                    .map(Integer::parseInt)
                    .filter(i -> (i >= 0 && i <= 255))
                    .count() == 4;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void setPreferenceChangeListener(@Nullable Preference preference) {
        if (preference != null) {
            preference.setOnPreferenceChangeListener(this);
            if (preference instanceof EditTextPreference) {
                onPreferenceChange(preference,
                        PreferenceManager
                                .getDefaultSharedPreferences(preference.getContext())
                                .getString(preference.getKey(), ""));
            } else if (preference instanceof SwitchPreference){
                onPreferenceChange(preference,
                        PreferenceManager
                                .getDefaultSharedPreferences(preference.getContext())
                                .getBoolean(preference.getKey(), false));
            }
        }
    }

    public PrefsFragment() {
        // Required empty public constructor
    }
}
