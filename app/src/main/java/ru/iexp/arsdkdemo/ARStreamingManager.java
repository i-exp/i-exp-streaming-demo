package ru.iexp.arsdkdemo;

import android.app.Application;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import org.webrtc.SurfaceViewRenderer;
import java.io.IOException;
import ru.iexp.arstreamingsdk.ARStreamingConfig;
import ru.iexp.arstreamingsdk.ARStreamingRenderer;
import ru.iexp.arstreamingsdk.ARStreamingSession;
import ru.iexp.arstreamingsdk.ARStreamingSessionListener;
import ru.iexp.arstreamingsdk.exceptions.InternalErrorException;

public class ARStreamingManager {
	private static ARStreamingManager instance;
	private static final String TAG = ARStreamingManager.class.getName();
    public enum StreamingStatus {
    	CONNECTING,
	    CONNECTED,
	    STREAMING,
	    PEER_CONNECTION_ERROR,
	    DISCONNECTED,
	    ERROR
    }
    private MutableLiveData<StreamingStatus> streamingStatus;
    private ARStreamingSessionListener listener;
    private ARStreamingSession session;
    private ARStreamingRenderer renderer;
    private Application application;

    private boolean busy = false;

	public static synchronized ARStreamingManager getInstance(Application application) {
		if (instance == null) {
			instance = new ARStreamingManager(application);
		}
		return instance;
	}
    public LiveData<StreamingStatus> getStreamingStatus() {
		return streamingStatus;
    }

    public void sendUserCommand(String command){
		session.sendUserCommand(command);
	}

	public void placeAnchorAtTap(MotionEvent ev){
		renderer.placeAnchorAtTap(ev);
	}

    public void initSession(ARStreamingConfig config) {
		if (busy){
			// it's up to user to prevent spamming session instances
			Log.e(TAG, "Session manager is busy");
			return;
		}
		busy = true;
	    session = new ARStreamingSession(config);
	    streamingStatus.postValue(StreamingStatus.CONNECTING);
	    session.initSession(listener);
    }

	private ARStreamingManager(Application application) {
		this.application = application;
		streamingStatus = new MutableLiveData<>();
		listener = new ARStreamingSessionListener() {
			@Override
			public void onSessionEstablished() {
				streamingStatus.postValue(StreamingStatus.CONNECTED);
			}

			@Override
			public void onSessionEstablishmentError(Exception e) {
                streamingStatus.postValue(StreamingStatus.ERROR);
				busy = false;
			}

            @Override
			public void onSessionConnectionError() {
                streamingStatus.postValue(StreamingStatus.PEER_CONNECTION_ERROR);
                busy = false;
			}

			@Override
			public void onSessionDisconnect() {
				streamingStatus.postValue(StreamingStatus.DISCONNECTED);
				busy = false;
			}
		};
	}

	public void startRendering(GLSurfaceView surfaceView, SurfaceViewRenderer remoteVideoView) {
		try {
			renderer = new ARStreamingRenderer(session, surfaceView, remoteVideoView);
			streamingStatus.postValue(StreamingStatus.STREAMING);
		} catch (IOException e) {
			Log.e(TAG,"ERROR CREATING RENDERER");
			streamingStatus.postValue(StreamingStatus.ERROR);
		}
	}

	public void onActivityPause() {
		if (renderer == null)
			return;
		renderer.onActivityPause();
	}

	public void onActivityDestroy() {
		if (renderer == null)
			return;
		renderer.onActivityDestroy();
	}

	public void onActivityResume() {
		if (renderer == null)
			return;
		renderer.onActivityResume();
	}
}
