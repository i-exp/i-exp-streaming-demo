package ru.iexp.arsdkdemo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.Toast;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import org.webrtc.SurfaceViewRenderer;

import java.util.Locale;

import ru.iexp.arsdkdemo.viewmodels.ARActivityViewModel;


/* TODO fix errors logging:
        2020-01-15 15:48:14.229 17362-17559/ru.iexp.arclient E/ACameraMetadata: getConstEntry: cannot find metadata tag 65576
        2020-01-15 15:48:14.229 17362-17559/ru.iexp.arclient E/ACameraMetadata: getConstEntry: cannot find metadata tag 65578
        ...
    Even if they seem to have no effect on functioning
*/
public class ARActivity extends AppCompatActivity {
    private static final String TAG = "ARC_ARActivity";
    private GLSurfaceView surfaceView;
    SurfaceViewRenderer remoteVideoView;
    private ARActivityViewModel viewModel;

    private int displayMode = 0;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.w(TAG, "OnCreate");
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_ar);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.hide();
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Log.d(TAG,"call surface view");

        Log.d(TAG,"create viewmodel");
        viewModel = new ViewModelProvider(this).get(ARActivityViewModel.class);
	    surfaceView = findViewById(R.id.surfaceview);
	    remoteVideoView = findViewById(R.id.remote_gl_surface_view);

        viewModel.getStreamingStatus().observe(this, ((ARStreamingManager.StreamingStatus streamingStatus) -> {
        	switch (streamingStatus) {
		        case ERROR: {
		        	showToast("Error occured");
		        	break;
		        }
		        case CONNECTED: {
		        	showToast("Starting rendering");
			        viewModel.startRendering(surfaceView, remoteVideoView);
		        	break;
		        }
		        case PEER_CONNECTION_ERROR: {
		        	showToast("Peer connection error");
		        	Intent intent  = new Intent(this, MainActivity.class);
		        	startActivity(intent);
		        	break;
		        }
		        case DISCONNECTED: {
		        	showToast("Disconnected");
		        	break;
		        }
	        }
        }));

        final GestureDetector gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public void onLongPress(MotionEvent e) {
                Log.v(TAG, "Handling long press");
                if (e != null) {
                    if (displayMode >= 3) {
                        displayMode = 0;
                    } else {
                        displayMode++;
                    }
                    viewModel.sendUserCommand(String.format(Locale.ENGLISH,"%d", displayMode));
                }

            }
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                Log.v(TAG, "Handling single tap");
                viewModel.placeAnchorAtTap(e);
                return true;
            }
           @Override
           public boolean onDown(MotionEvent e) {
               return true;
           }
        });

        surfaceView.setOnTouchListener((v, event) -> gestureDetector.onTouchEvent(event));
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.onActivityResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewModel.onActivityPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.onActivityDestroy();
    }

	public void showToast(final String msg) {
		runOnUiThread(() -> Toast.makeText(this, msg, Toast.LENGTH_SHORT).show());
	}
}
