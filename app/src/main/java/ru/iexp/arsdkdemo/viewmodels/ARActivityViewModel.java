package ru.iexp.arsdkdemo.viewmodels;

import android.app.Application;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import org.webrtc.SurfaceViewRenderer;
import ru.iexp.arsdkdemo.ARStreamingManager;

public class ARActivityViewModel extends AndroidViewModel {
	private static final String TAG = ARActivityViewModel.class.getName();
	private ARStreamingManager manager;

	public ARActivityViewModel(Application application) {
		super(application);
		manager = ARStreamingManager.getInstance(application);
	}

	public LiveData<ARStreamingManager.StreamingStatus> getStreamingStatus() {
		return manager.getStreamingStatus();
	}

	public void startRendering(GLSurfaceView surfaceView, SurfaceViewRenderer remoteVideoView) {
		manager.startRendering(surfaceView, remoteVideoView);
	}

	public void onActivityPause() {
		manager.onActivityPause();
	}

	public void sendUserCommand(String command){
		manager.sendUserCommand(command);
	}

	public void placeAnchorAtTap(MotionEvent ev){
		manager.placeAnchorAtTap(ev);
	}

	public void onActivityDestroy() {
		manager.onActivityDestroy();
	}

	public void onActivityResume() {
		manager.onActivityResume();
	}


}


