package ru.iexp.arsdkdemo.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import ru.iexp.arsdkdemo.ARStreamingManager;
import ru.iexp.arstreamingsdk.ARStreamingConfig;

public class MainActivityViewModel extends AndroidViewModel {
	private static final String TAG = MainActivityViewModel.class.getName();
	private ARStreamingManager manager;

	public MainActivityViewModel(Application application) {
		super(application);
		manager = ARStreamingManager.getInstance(application);
	}

	public LiveData<ARStreamingManager.StreamingStatus> getStreamingStatus() {
		return manager.getStreamingStatus();
	}

	public void initSession(ARStreamingConfig config) {
		manager.initSession(config);
	}
}

