package ru.iexp.arsdkdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.net.InetSocketAddress;
import java.util.Optional;
import ru.iexp.arsdkdemo.viewmodels.MainActivityViewModel;
import ru.iexp.arstreamingsdk.ARStreamingConfig;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "ARC_Main";
    private MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
	    //check camera persmission
	    if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
		    ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, 0 );
	    }

        Button btnStart = findViewById(R.id.btn_start);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.prefsPlaceholder, new PrefsFragment())
                .commit();

        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        viewModel.getStreamingStatus().observe(this, ((ARStreamingManager.StreamingStatus streamingStatus) -> {
        	switch (streamingStatus) {//make some animations or etc when in connecting state
		        case CONNECTED: {
			        showToast("Connected!");
			        Intent intent = new Intent(this, ARActivity.class);
			        startActivity(intent);
			        break;
		        }
		        case CONNECTING: {
		        	showToast("Connecting to server");
		        	break;
		        }
		        case ERROR: {
		        	showToast("Error during session establishment");
		        	break;
		        }
	        }
        }));

        btnStart.setOnClickListener((View v) -> {
        	try {
        		Log.d("ARStreaming", "button pressed");
        		ARStreamingConfig config = createConfig();
        		viewModel.initSession(config);
        	} catch (WrongParametersException e) {
        		showToast("Wrong parameters");
        	}
        });

    }

    private ARStreamingConfig createConfig() throws WrongParametersException {
    	ARStreamingConfig.Builder builder;
	    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
	    String serverSocket = sp.getString(getString(R.string.pref_manager_socket), "");

	    Optional<InetSocketAddress> address = stringToAddress(serverSocket);

	    if (!address.isPresent())
	    	throw new WrongParametersException();

	    builder = new ARStreamingConfig.Builder(address.get(), getApplicationContext());
	    if (isSettingEnabled(R.string.pref_use_qr_code, sp))
	    	builder.withImageRecognition(new String[] {"bitmaps/iexp_qr.png"});

	    if (isSettingEnabled(R.string.pref_dynamic_tracking, sp))
	    	builder.withUseContiniousTracking();

	    if (isSettingEnabled(R.string.pref_align_anchor, sp))
	    	builder.withAlignAnchor();

	    if (isSettingEnabled(R.string.pref_use_autofocus, sp))
	    	builder.withAutoFocus();

	    if (isSettingEnabled(R.string.pref_use_lighting, sp))
	    	builder.withLightingEstimation();

	    return builder
			    .build();
    }

    private boolean isSettingEnabled(int resID, SharedPreferences sp) {
    	return sp.getBoolean(getString(resID), false);
    }

    private Optional<InetSocketAddress> stringToAddress(String address) {
	    int dividerPos = address.indexOf(":");
	    if (dividerPos == -1)
	    	return Optional.empty();

	    String signalingIP = address.substring(0, dividerPos);
	    int signalingPort = Integer.valueOf(address.substring(dividerPos + 1));
	    return Optional.of(new InetSocketAddress(signalingIP, signalingPort));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void showToast(final String msg) {
        runOnUiThread(() -> Toast.makeText(this, msg, Toast.LENGTH_SHORT).show());
    }
}
